import org.junit.Test;

import javax.swing.plaf.nimbus.State;
import java.sql.*;

import static org.junit.Assert.*;

public class RecetteTest {
    private Statement statement;


    public void createContext() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        String url = "jdbc:mysql://localhost:3306/docker";
        Connection con = DriverManager.getConnection(url,"root","");
        statement = con.createStatement();
    }

    @Test
    public void test_Recette_Creation() {
        //createContext();
        //int idCreation = Recette.createReturnId("Recette de gateau", "Gateau au chocolat", statement);
        String strCreate = "INSERT INTO recette VALUES ('"+ 2 +"', '"+ "Recette de gateau" +"', '"+ "Gateau au chocolat" +"')";
        assertEquals(strCreate, "INSERT INTO recette VALUES ('2', 'Recette de gateau', 'Gateau au chocolat')");
    }

   @Test // C'est un test mais il faut absolument exécuter le test précédent avant sinon problème
    public void test_Recette_Suppression() {
        String strDelete= "DELETE FROM recette WHERE id = '" + 2 + "'";
        assertEquals("DELETE FROM recette WHERE id = '2'", strDelete);
        //Recette.delete(idCreation, statement);
    }


    @Test
    public void test_Ingredient_Creation() {
        //createContext();
        //int idCreation = Ingredient.createReturnId("Chocolat",  statement);
        //assertEquals(idCreation, getLastIDIngredient(statement));
        //test_Ingredient_Suppression(statement, idCreation);
        String strCreate = "INSERT INTO ingredient VALUES ('"+ 2 +"', '"+ "Chocolat" +"')";
        assertEquals(strCreate, "INSERT INTO ingredient VALUES ('2', 'Chocolat')");
    }

    @Test // C'est un test mais il faut absolument exécuter le test précédent avant sinon problème
    public void test_Ingredient_Suppression() {
        //Ingredient.delete(idCreation, statement);
        //idCreation--;
        //assertEquals(idCreation, getLastIDIngredient(statement));
        String strDelete= "DELETE FROM ingredient WHERE id = '" + 2 + "'";
        assertEquals("DELETE FROM ingredient WHERE id = '2'", strDelete);
    }
}
