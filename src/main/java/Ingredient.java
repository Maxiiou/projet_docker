import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Ingredient {
    int id;
    String nom;

    public Ingredient(int id, String nom) {
        this.id = id;
        this.nom = nom;
    }

    public static void create(String nom,Statement statement) throws SQLException {
        String strQuery = "SELECT ingredient.id from ingredient order by ingredient.id DESC LIMIT 1";
        ResultSet rsRecette = statement.executeQuery(strQuery);
        rsRecette.next();
        int id = rsRecette.getInt(1) + 1;

        String strInsert = "INSERT INTO ingredient VALUES ('"+id+"','"+nom+"'); ";
        statement.executeUpdate(strInsert);
        System.out.println("L'ingrédient a été crée");
    }

    public static int createReturnId(String nom,Statement statement) throws SQLException {
        String strQuery = "SELECT ingredient.id from ingredient order by ingredient.id DESC LIMIT 1";
        ResultSet rsRecette = statement.executeQuery(strQuery);
        rsRecette.next();
        int id = rsRecette.getInt(1) + 1;

        String strInsert = "INSERT INTO ingredient VALUES ('"+id+"','"+nom+"'); ";
        statement.executeUpdate(strInsert);
        System.out.println("L'ingrédient a été crée");

        return id;
    }

    public static ArrayList<Ingredient> selectAllId(int id, Statement statement) throws SQLException {
        ArrayList<Ingredient> i = new ArrayList<>();
        String strIngredient = "SELECT ingredient.id, ingredient.nom FROM ingredient\n" +
                "JOIN recettes_ingredients ON ingredient.id = recettes_ingredients.id_Ingredient\n" +
                "JOIN recette ON recettes_ingredients.id_Recette = recette.id\n" +
                "WHERE recette.id ="+id;
        ResultSet rsIngredient = statement.executeQuery(strIngredient);
        while(rsIngredient.next()) {
            i.add(new Ingredient(rsIngredient.getInt(1), rsIngredient.getString(2)));
        }
        return i;
    }
    public static void delete(int id, Statement statement) throws SQLException {
        String strDelete= "DELETE FROM ingredient WHERE id = "+id;
        statement.executeUpdate(strDelete);

        strDelete = "DELETE FROM recettes_ingredients WHERE id_Ingredient = "+id;
        statement.executeUpdate(strDelete);
    }

    @Override
    public String toString() {
        return "\n Ingredient{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                '}';
    }
}
