import java.sql.*;
import java.util.ArrayList;
import java.util.Scanner;

public class main {

    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        String url = "jdbc:mysql://localhost:3306/docker";
        Connection con = DriverManager.getConnection(url,"root","");
        Statement statement = con.createStatement();
        Scanner scanner = new Scanner(System.in).useDelimiter("\n");
        int responseint = 0;
        String nom = "";
        String description = "";
        int idIngredient = 0;
        int idRecette = 0;

        System.out.println("Connection Established");
        System.out.println("Voici les recettes avec les ingrédients : ");
        ArrayList<Recette> recettes = Recette.selectNomWithIngredient("i",statement);
        for(Recette r : recettes){
            System.out.println(r.toString());
        }

        while(responseint != 0){
            System.out.println("voulez vous : ");
            System.out.println("1 = Créer une recette ");
            System.out.println("2 = Créer un ingrédient ");
            System.out.println("3 = Associer un ingredient à une recette");
            System.out.println("4 = Supprimer une recette");
            System.out.println("5 = Supprimer un ingrédient");
            System.out.println("6 = Supprimer un ingrédient d'une recette");
            System.out.println("7 = Chercher des recettes avec un nom");
            System.out.println("0 = Quitter");
            responseint = scanner.nextInt();
            switch (responseint){
                case 1 :
                    System.out.println("Vous aller créer une recette...");
                    System.out.println("Nom de la recette ?");
                    nom = scanner.next();
                    System.out.println("Description de la recette");
                    description = scanner.next();
                    Recette.create(nom,description,statement);
                    break;
                case 2 :
                    System.out.println("Vous aller créer un ingrédient...");
                    System.out.println("Le nom de l'ingrédient ? ");
                    nom = scanner.next();
                    Ingredient.create(nom,statement);
                    break;
                case 3 :
                    System.out.println("Vous aller associer un ingrédient à une recette...");
                    System.out.println("L'id de l'ingrédient : ");
                    idIngredient = scanner.nextInt();
                    System.out.println("L'id de la recette : ");
                    idRecette = scanner.nextInt();

                    Recette.associateRecetteAndIngredient(idRecette,idIngredient,statement);

                    break;
                case 4 :
                    System.out.println("Vous aller supprimer une recette...");
                    System.out.println("L'id de la recette : ");
                    idRecette = scanner.nextInt();
                    Recette.delete(idRecette,statement);

                    break;
                case 5 :
                    System.out.println("Vous aller supprimer un ingrédient...");
                    System.out.println("L'id de l'ingrédient : ");
                    idIngredient = scanner.nextInt();
                    Ingredient.delete(idIngredient,statement);

                    break;
                case 6 :
                    System.out.println("Vous aller supprimer un ingrédient d'une recette");
                    System.out.println("Id de la recette : ");
                    idRecette = scanner.nextInt();
                    System.out.println("Id de l'ingrédient : ");
                    idIngredient = scanner.nextInt();
                    Recette.deletteIngredient(idRecette,idIngredient,statement);

                    break;
                case 7 :
                    System.out.println("Vous aller rechercher une recette à partir de son nom...");
                    System.out.println("Nom de la recette :");
                    nom = scanner.next();
                    ArrayList<Recette> re = Recette.selectNomWithIngredient(nom,statement);

                    for(Recette r : re){
                        System.out.println(re.toString());
                    }
                    break;
                default :
                    System.out.println("Fonctionne pas");
                    break;
            }
            System.out.println("Voici les recettes avec les ingrédients : ");
            recettes = Recette.selectNomWithIngredient("i",statement);
            for(Recette r : recettes){
                System.out.println(r.toString());
            }
        }
    }
}
