import javax.swing.plaf.nimbus.State;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Recette {
    int id;
    String nom;
    String description;
    ArrayList<Ingredient> ingredients;

    public Recette(int id, String nom, String description) {
        this.id = id;
        this.nom = nom;
        this.description = description;
        ingredients = new ArrayList<>();
    }

    public static ArrayList<Recette> selectAll(Statement statement) throws SQLException {
        ArrayList<Recette> recettes = new ArrayList<>();

        String strQuery = "SELECT * FROM recette;";
        ResultSet rsRecette = statement.executeQuery(strQuery);

        while(rsRecette.next()) {
            recettes.add(new Recette(rsRecette.getInt(1), rsRecette.getString(2), rsRecette.getString(3)));
        }
        return recettes;
    }

    public static ArrayList<Recette> selectNomWithIngredient(String nom,Statement statement) throws SQLException {
        ArrayList<Recette> recettes = new ArrayList<>();
        String strQuery = "SELECT * FROM recette WHERE nom LIKE '%"+nom+"%' ; ";
        ResultSet rsRecette = statement.executeQuery(strQuery);

        while(rsRecette.next()) {
            recettes.add(new Recette(rsRecette.getInt(1), rsRecette.getString(2), rsRecette.getString(3)));
        }
        for (Recette r: recettes){
            r.setIngredients(Ingredient.selectAllId(r.id,statement));
        }

        return recettes;
    }

    public static ArrayList<Recette> selectAllWithIngredient(Statement statement) throws SQLException {
        ArrayList<Recette> recettes = Recette.selectAll(statement);

        for(Recette r : recettes){
            r.setIngredients(Ingredient.selectAllId(r.id,statement));
        }
        return recettes;
    }

    public void setIngredients(ArrayList<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public static void delete(int id, Statement statement) throws SQLException {
        String strDelete= "DELETE FROM recette WHERE id = "+id;
        statement.executeUpdate(strDelete);

        strDelete = "DELETE FROM recettes_ingredients WHERE id_Recette = "+id;
        statement.executeUpdate(strDelete);
    }

    public static void deletteIngredient(int recetteId,int IngredientId,Statement statement) throws SQLException {
        String strQueryRecette = "SELECT *  FROM recette\n" +
                "JOIN recettes_ingredients ON recette.id = recettes_ingredients.id_Recette\n" +
                "WHERE id_Recette = "+recetteId+" AND id_Ingredient = "+IngredientId;
        ResultSet rsRecette = statement.executeQuery(strQueryRecette);
        rsRecette.next();
        if (rsRecette.getRow() >= 1){
            strQueryRecette = "DELETE FROM recettes_ingredients WHERE id_Recette = "+recetteId+" AND id_Ingredient = "+IngredientId;
            statement.executeUpdate(strQueryRecette);
            System.out.println("L'ingrédient a été correctement supprimé");
        }
        else{
            System.out.println("existe pas");
        }
    }

    public static void create(String nom,String description,Statement statement) throws SQLException {
        String strQuery = "SELECT recette.id from recette order by recette.id DESC LIMIT 1";
        ResultSet rsRecette = statement.executeQuery(strQuery);
        rsRecette.next();
        int id = rsRecette.getInt(1) + 1;


        String strInsert = "INSERT INTO recette VALUES ('"+id+"','"+nom+"','"+description+"');";
        statement.executeUpdate(strInsert);
        System.out.println("Recette crée !");
    }

    public static int createReturnId(String nom,String description,Statement statement) throws SQLException {
        String strQuery = "SELECT recette.id from recette order by recette.id DESC LIMIT 1";
        ResultSet rsRecette = statement.executeQuery(strQuery);
        rsRecette.next();
        int id = rsRecette.getInt(1) + 1;


        String strInsert = "INSERT INTO recette VALUES ('"+id+"','"+nom+"','"+description+"');";
        statement.executeUpdate(strInsert);
        System.out.println("Recette crée !");

        return id;
    }

    public static void associateRecetteAndIngredient(int idRecette,int idIngredient,Statement s) throws SQLException {
        String strQueryRecette = "SELECT * FROM recette WHERE id = "+idRecette+";";
        ResultSet rsRecette = s.executeQuery(strQueryRecette);
        rsRecette.next();
        if (rsRecette.getRow() >= 1){
            String strQueryIngredient = "SELECT * FROM ingredient WHERE id = "+idIngredient+";";
            ResultSet rsIngredient = s.executeQuery(strQueryRecette);
            rsIngredient.next();
            if (rsIngredient.getRow() >= 1){
                String strInsert = "INSERT INTO recettes_ingredients VALUES("+idRecette+","+idIngredient+");";
                s.executeUpdate(strInsert);
                System.out.println("La recette a bien été associé à l'ingrédient");
            }
            else{
                System.out.println("L'ingrédient n'existe pas");
            }
        }
        else{
            System.out.println("La Recette n'existe pas");
        }
    }



    @Override
    public String toString() {
        StringBuilder str = new StringBuilder("Recette{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", description='" + description + '\'');

        for(Ingredient e : ingredients){
            str.append(e.toString());
        }
        str.append("}");
        return str.toString();
    }
}
