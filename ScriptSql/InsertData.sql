-- Création de données
INSERT INTO recette VALUES(1, 'Recette de Cookies', 'Pour cette recette vous devez mélanger la farine et la levure puis mélanger le sucre et le sucre vanillé, mélanger le tout. Ensuite ajouter le beurre, puis les oeufs. Enfin ajouter les pépites de chocolats et faite cuire à 220°C pendant 10 minutes');

INSERT INTO ingredient VALUES(1,'150g de Farine');
INSERT INTO ingredient VALUES(2,'1 demi-sachet de Levure Chimique');
INSERT INTO ingredient VALUES(3,'85g de Sucre');
INSERT INTO ingredient VALUES(4,'1 sachet de sucre vanillé');
INSERT INTO ingredient VALUES(5,'1 pincé de sel');
INSERT INTO ingredient VALUES(6,'1 oeuf');
INSERT INTO ingredient VALUES(7,'85g de beurre');
INSERT INTO ingredient VALUES(8,'100g de pépites de chocolats');

INSERT INTO recettes_Ingredients VALUES(1, 1);
INSERT INTO recettes_Ingredients VALUES(1, 2);
INSERT INTO recettes_Ingredients VALUES(1, 3);
INSERT INTO recettes_Ingredients VALUES(1, 4);
INSERT INTO recettes_Ingredients VALUES(1, 5);
INSERT INTO recettes_Ingredients VALUES(1, 6);
INSERT INTO recettes_Ingredients VALUES(1, 7);
INSERT INTO recettes_Ingredients VALUES(1, 8);