create SCHEMA recettes;
use recettes;

-- create tables
CREATE TABLE ingredient (
id INT,
nom VARCHAR(255),
    PRIMARY KEY (id)
);

CREATE TABLE recette (
id INT,
nom varchar(255),
description varchar(255),
    PRIMARY KEY (id)
);

CREATE TABLE recettes_Ingredients (
id_Recette INT,
    INDEX rec_ind (id_Recette),
    FOREIGN KEY (id_Recette)
        REFERENCES recette(id)
        ON DELETE CASCADE,
id_Ingredient INT,
    INDEX ing_ind (id_Ingredient),
    FOREIGN KEY (id_Ingredient)
        REFERENCES ingredient(id)
        ON DELETE CASCADE
);